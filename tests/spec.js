describe('Portal estadísticas Tests Interfaz', function() {
	
  var abonadosButton=element(by.css('.buttonAbonados'));
  
  var abonadosPrevisionButton=element(by.css('.buttonAbonadosPrevision'));
  
  var numerosButton=element(by.css('.buttonNumeros'));
  
  var numerosPrevisionButton=element(by.css('.buttonNumerosPrevision'));
  
  var selectEmpresaAbonadosPrevision=element(by.id('empresaAbonados'));
  
  var selectEmpresaNumeros=element(by.id('empresaNumeros'));
  
  var selectEmpresaNumerosPrevision=element(by.id('empresaNumerosPrevision'));
  
  var includeElem = element(by.css('[ng-include]'));
  
  var selectModoPagoNumeros=element(by.id('modoPago'));
  
  var selectModoPagoNumerosPrevision=element(by.id('modoPagoPrevision')); 
  
  var selectTipoTerminalNumeros=element(by.id('tipoTerminal'));
  
  var selectModoPagoNumerosPrevision=element(by.id('modoPagoPrevision'));
  
  
  beforeEach(function() {
    browser.get('http://127.0.0.1:64865/');
  });	
	
  it('URL principal Load', function() {
    expect(browser.getTitle()).toEqual('Estadísticas SGDA');
  });
  
  
  it('Click en "Numeros" y carga el template correcto', function() {
	numerosButton.click();	
    expect(includeElem.isPresent()).toBe(true);
	expect(includeElem.evaluate('activeTemplate')).toBe('pages/ContratoPrepagoTodos.html');
  });
  
  it('Click en "Abonados Previsión de carga" y carga el template correcto', function() {
	abonadosPrevisionButton.click();	
    expect(includeElem.isPresent()).toBe(true);
	expect(includeElem.evaluate('activeTemplate')).toBe('pages/CargadosPrevistosEmpresas.html');
  });
  
  it('Click en "Numeros Previsión de carga" y carga el template correcto', function() {
	numerosPrevisionButton.click();	
    expect(includeElem.isPresent()).toBe(true);
	expect(includeElem.evaluate('activeTemplate')).toBe('pages/CargadosPrevistosEmpresas.html');
  });
  
  it('Click en "Abonados" no debe cargar un template', function() {
	abonadosButton.click();	
    expect(includeElem.isPresent()).toBe(false);
  });
  
  it('Carga gráfico circular correcto en sección Abonados', function() {
	abonadosButton.click();	
	var chartInsertado = element(by.css('[chart=chartObject]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección Numeros con Empresa "TODAS" y Modo de pago "TODOS"', function() {
	numerosButton.click();	
	var chartInsertado = element(by.css('[chart=chartColumnsObjectContrato]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Abonados Previsión de carga" con Empresa "TODAS"', function() {
	abonadosPrevisionButton.click();	
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Números Previsión de carga" con Empresa "TODAS" y modo de pago "TODOS"', function() {
	numerosPrevisionButton.click();	
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  
  it('Carga gráfico circular correcto en sección "Abonados Previsión de carga" con Empresa Única', function() {
	abonadosPrevisionButton.click();	
	selectEmpresaAbonadosPrevision.click();
	var elementoEmpresa=selectEmpresaAbonadosPrevision.element(by.css('option[value="11"]'));
	elementoEmpresa.click();
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresa]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números" con Empresa Única y Modo de Pago "TODOS"', function() {
	numerosButton.click();		
	selectEmpresaNumeros.click();
	var elementoEmpresa=selectEmpresaNumeros.element(by.css('option[value="11"]'));
	elementoEmpresa.click();
	var chartInsertado = element(by.css('[chart=chartObjectContratoPrepagoEmpresa]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números" con Empresa "TODAS" y Modo de Pago "Contrato"', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value="Contrato"]'));
	elementoModo.click();
	var chartInsertado = element(by.css('[chart=chartObjectContrato]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números" con Empresa "TODAS" y Modo de Pago "Prepago"', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value="Prepago"]'));
	elementoModo.click();
	var chartInsertado = element(by.css('[chart=chartObjectPrepago]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Desbloquear filtro Tipo de Terminal poniendo Modo de pago en " -- No Filtrar --"', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value=" -- No filtrar -- "]'));
	elementoModo.click();
	expect(selectTipoTerminalNumeros.isEnabled());
  });
  
  it('Bloquear Modo de pago poniendo seleccionando un Tipo de Terminal o TODOS', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value=" -- No filtrar -- "]'));
	elementoModo.click();
	var elementoTipo=selectTipoTerminalNumeros.element(by.css('option[value="Terminal videotex"]'));
	elementoTipo.click();	
    expect(selectModoPagoNumeros.isEnabled()).toBe(false);;
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Números" con Empresa "TODAS" y Tipo de Terminal "TODOS"', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value=" -- No filtrar -- "]'));
	elementoModo.click();
	var elementoTipo=selectTipoTerminalNumeros.element(by.css('option[value="TODOS"]'));
	elementoTipo.click();	
    var chartInsertado = element(by.css('[chart=chartObjectTiposEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números" con Empresa "TODAS" y un determinado Tipo de Terminal', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value=" -- No filtrar -- "]'));
	elementoModo.click();
	var elementoTipo=selectTipoTerminalNumeros.element(by.css('option[value="Terminal videotex"]'));
	elementoTipo.click();	
    var chartInsertado = element(by.css('[chart=chartObjectTipoEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números" con una empresa determinada y un determinado Tipo de Terminal', function() {
	numerosButton.click();		
	selectModoPagoNumeros.click();
	var elementoModo=selectModoPagoNumeros.element(by.css('option[value=" -- No filtrar -- "]'));
	elementoModo.click();
	var elementoTipo=selectTipoTerminalNumeros.element(by.css('option[value="Terminal videotex"]'));
	elementoTipo.click();	
	var elementoTipo=selectTipoTerminalNumeros.element(by.css('option[value="Terminal videotex"]'));
	elementoTipo.click();	
    var chartInsertado = element(by.css('[chart=chartObjectTipoEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Números Prevision de Carga" con Empresa "TODAS" y Modo de Pago "TODOS"', function() {
	numerosPrevisionButton.click();		
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresas]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Números Prevision de Carga" con Empresa "TODAS" y un determinado Modo de Pago', function() {
	numerosPrevisionButton.click();		
	selectModoPagoNumerosPrevision.click();
	var elementoModo=selectModoPagoNumerosPrevision.element(by.css('option[value="Contrato"]'));
	elementoModo.click();	
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresa]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico de barras apiladas correcto en sección "Números Prevision de Carga" con una determinada Empresa y Modo de Pago "TODOS"', function() {
	numerosPrevisionButton.click();		
	selectModoPagoNumerosPrevision.click();
	var elementoEmpresa=selectEmpresaNumerosPrevision.element(by.css('option[value="11"]'));
	elementoEmpresa.click();	
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresa]'));
    expect(chartInsertado.isPresent());
  });
  
  it('Carga gráfico circular correcto en sección "Números Prevision de Carga" con una determinada Empresa y un determinado Modo de Pago', function() {
	numerosPrevisionButton.click();		
	selectModoPagoNumerosPrevision.click();
	var elementoEmpresa=selectEmpresaNumerosPrevision.element(by.css('option[value="11"]'));
	elementoEmpresa.click();	
	var elementoModo=selectModoPagoNumerosPrevision.element(by.css('option[value="Contrato"]'));
	elementoModo.click();	
	var chartInsertado = element(by.css('[chart=chartObjectCargadosPrevistosEmpresa]'));
    expect(chartInsertado.isPresent());
  });
});