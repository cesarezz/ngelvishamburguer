
describe('Modulo de Carta', function() {


	var 
		cartaService, 
		carta,
		accessPoint;

	beforeEach(angular.mock.module("elvisHamburguerApp"));

    beforeEach(inject(function (_cartaService_ , _carta_, _accessPoint_) {
		
		cartaService = _cartaService_;
		carta = _carta_;
		accessPoint = _accessPoint_;
		
	}));

	it('Debería pedir la carta (api rest) y rellenar la variable data de la factoria Carta con los resultados',inject(function($http,$httpBackend){

		var data = [{"id":1,"nombre":"Suspicious  Mind","descripcion":"maravillosa hamburguesa,¼ de libra, con \r\ncebolla a la plancha, bacon y queso.","precio":5.5},{"id":2,"nombre":"My Way","descripcion":"hamburguesa de ¼ de libra con \r\nlechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.","precio":4.0},{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95},{"id":4,"nombre":"Refresco","descripcion":"Coca-Cola o Seven-Up","precio":2.0},{"id":5,"nombre":"Chesse Fries","descripcion":"Patatas con queso cheddar y bacon","precio":3.0},{"id":6,"nombre":"Ingrediente Extra","descripcion":"A elegir entre relish  (salsa  de \r\npepinillo dulce picado), bacon y queso cheddar","precio":0.5},{"id":7,"nombre":"Refresco Refill","descripcion":"Coca-Cola o Seven-Up con Refill","precio":5.0}];

		$httpBackend.expectGET(accessPoint  + '/carta/').respond(200, data); //se espera un get de la carga de empresa por ID

		cartaService.pedirCarta();

		$httpBackend.flush();

	}));	

});

describe('Modulo de Crear', function() {

	var 
		crearService, 
		crear,
		accessPoint;

	beforeEach(angular.mock.module("elvisHamburguerApp"));

    beforeEach(inject(function (_crearService_ , _crear_, _accessPoint_) {
		
		crearService = _crearService_;
		crear = _crear_;
		accessPoint = _accessPoint_;
		
	}));

	it('Deberia resetear la factoria de creacion',inject(function(){

		var data = [{"id":1,"nombre":"Suspicious  Mind","descripcion":"maravillosa hamburguesa,¼ de libra, con \r\ncebolla a la plancha, bacon y queso.","precio":5.5},{"id":2,"nombre":"My Way","descripcion":"hamburguesa de ¼ de libra con \r\nlechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.","precio":4.0},{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95},{"id":4,"nombre":"Refresco","descripcion":"Coca-Cola o Seven-Up","precio":2.0},{"id":5,"nombre":"Chesse Fries","descripcion":"Patatas con queso cheddar y bacon","precio":3.0},{"id":6,"nombre":"Ingrediente Extra","descripcion":"A elegir entre relish  (salsa  de \r\npepinillo dulce picado), bacon y queso cheddar","precio":0.5},{"id":7,"nombre":"Refresco Refill","descripcion":"Coca-Cola o Seven-Up con Refill","precio":5.0}];

		//RELLENO LA FACTORIA 
		crear.newAlimentoPedido = {};
		crear.newAlimentoPedido.carta_id = 5;
		crear.newAlimentoPedido.nombre = "Hamburguesa";
		crear.newAlimentoPedido.precio = 5.3;
		crear.newAlimentoPedido.cantidad = 5;
		crear.newPedido = {};
		crear.newPedido.alimentos = [];
		crear.autocomplete.selectedItem = 1;
		crear.autocomplete.searchText = "Hamb";		
		crear.pedidos = [];
		var objetoPedido = {};
		objetoPedido.idPedido = 6;
		objetoPedido.alimentos = [];
		objetoPedido.tiempo = 0;
		objetoPedido.precio = 0;
		objetoPedido.pagado = false;
		crear.pedidos.push(objetoPedido);

		//EJECUTO LA FUNCION
		crearService.inicializarFactoria();

		//COMPRUEBO QUE SE VACÍAN LOS CAMPOS.
		expect(crear.newAlimentoPedido).toEqual({cantidad:null});
		expect(crear.newPedido).toEqual({});
		expect(crear.autocomplete.selectedItem).toBe(null);
		expect(crear.autocomplete.searchText).toBe("");
		expect(crear.newAlimentoPedido.cantidad).toBe(null);
		expect(crear.pedidos).toEqual([]);

	}));

	it('Debería pedir la carta (api rest) y rellenar la variable carta de la factoria Crear con los resultados',inject(function($http,$httpBackend){

		var data = [{"id":1,"nombre":"Suspicious  Mind","descripcion":"maravillosa hamburguesa,¼ de libra, con \r\ncebolla a la plancha, bacon y queso.","precio":5.5},{"id":2,"nombre":"My Way","descripcion":"hamburguesa de ¼ de libra con \r\nlechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.","precio":4.0},{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95},{"id":4,"nombre":"Refresco","descripcion":"Coca-Cola o Seven-Up","precio":2.0},{"id":5,"nombre":"Chesse Fries","descripcion":"Patatas con queso cheddar y bacon","precio":3.0},{"id":6,"nombre":"Ingrediente Extra","descripcion":"A elegir entre relish  (salsa  de \r\npepinillo dulce picado), bacon y queso cheddar","precio":0.5},{"id":7,"nombre":"Refresco Refill","descripcion":"Coca-Cola o Seven-Up con Refill","precio":5.0}];

		$httpBackend.expectGET(accessPoint  + '/carta/').respond(200, data); //se espera un get de la carga de empresa por ID

		crearService.pedirCarta();

		$httpBackend.flush();

	}));	

    it('Funcion QueryBusqueda: esta funcion recibe texto y devuelve los elementos de la carta que contengan dicho texto.',inject(function(){

		//Relleno la carta
		crear.carta = [{"id":1,"nombre":"Suspicious  Mind","descripcion":"maravillosa hamburguesa,¼ de libra, con \r\ncebolla a la plancha, bacon y queso.","precio":5.5},{"id":2,"nombre":"My Way","descripcion":"hamburguesa de ¼ de libra con \r\nlechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.","precio":4.0},{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95},{"id":4,"nombre":"Refresco","descripcion":"Coca-Cola o Seven-Up","precio":2.0},{"id":5,"nombre":"Chesse Fries","descripcion":"Patatas con queso cheddar y bacon","precio":3.0},{"id":6,"nombre":"Ingrediente Extra","descripcion":"A elegir entre relish  (salsa  de \r\npepinillo dulce picado), bacon y queso cheddar","precio":0.5},{"id":7,"nombre":"Refresco Refill","descripcion":"Coca-Cola o Seven-Up con Refill","precio":5.0}];

		//Defino el texto de entrada
		var texto = "cake";

		//Array esperado
		var arrayDevolucion = [{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95}];
		

		var result = crearService.queryBusqueda(texto);

		expect(result).toEqual(arrayDevolucion);

	}));	

	it('Funcion selectedItemChange: esta funcion recibe un objeto nuevo alimento y se encarga de colocar el elemento en la factoria de creacion (newAlimentoPedido)',inject(function(){

		//Defino el objeto de entrada
		var objetoAlimento = {"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95};

		crearService.selectedItemChange(objetoAlimento);

		expect(crear.newAlimentoPedido.carta_id).toBe(3);
		expect(crear.newAlimentoPedido.nombre).toBe("New  York  cheese  cake");
		expect(crear.newAlimentoPedido.precio).toBe(4.95);

	}));

	it('Funcion addAlimentoPedido: Adjunta en newPedido de la factoria de creacion el alimento que hubiera en newAlimentoPedido de la factoria de creacion. ',inject(function(){

		//Añado un alimento a newAlimentoPedido
		crear.newAlimentoPedido.carta_id = 3;
		crear.newAlimentoPedido.nombre = "New  York  cheese  cake";
		crear.newAlimentoPedido.precio = 4.95;
		crear.newAlimentoPedido.cantidad = 5;
		

		crear.newPedido.alimentos = [];

		//Añado un alimento que no estaba en el array Alimentos (inicializado vacio)
		crearService.addAlimentoPedido();

		//Compruebo que este el elemento y tenga 5 de cantidad
		expect(crear.newPedido.alimentos.length).toBe(1);
		expect(crear.newPedido.alimentos[0].cantidad).toBe(5);

		//Añado un alimento a newAlimentoPedido (REPITO EL MISMO ALIMENTO CON 6 cantidad)
		crear.newAlimentoPedido.carta_id = 3;
		crear.newAlimentoPedido.nombre = "New  York  cheese  cake";
		crear.newAlimentoPedido.precio = 4.95;
		crear.newAlimentoPedido.cantidad = 6;

		//Añado un alimento que YA esta en el array Alimentos
		crearService.addAlimentoPedido();

		//Compruebo que este el elemento y tenga la suma de la cantidad inicial y la nueva (11)
		expect(crear.newPedido.alimentos.length).toBe(1);
		expect(crear.newPedido.alimentos[0].cantidad).toBe(11);

	}));





	it('Funcion enviarPedido: Peticion rest para recibir un id de pedido nuevo. Se realizan varias peticiones rest en subfunciones, también analizadas',inject(function($httpBackend){

		//Defino los campos que necesita la funcion
		crear.newAlimentoPedido.carta_id = 3;
		crear.newAlimentoPedido.nombre = "New  York  cheese  cake";
		crear.newAlimentoPedido.precio = 4.95;
		crear.newAlimentoPedido.cantidad = 6;

		crear.newPedido.alimentos = [];

		crear.newPedido.alimentos.push(crear.newAlimentoPedido);


		//Defino el resultado esperado de /pedido/new GET
		var data = {"id": 326,"mesa": null,"alimentos_pedido": [0],"pagado": false,"tiempo": null,"precio": null};

		//Defino el resultado esperado de /alimentosPedido/new POST
		var objetoPost = {};
		objetoPost.alimentosPedidos = crear.newPedido.alimentos;

		//Defino el resultado esperado de /pedido/{idPedido}}/tiempo/{tiempo}/precio/{precio}  GET
		var idPedido = 326;
		var tiempo = 600;
		var precio = 29.700000000000003;

		$httpBackend.expectGET(accessPoint  + '/pedido/new/').respond(200,data); //se espera un get 
		$httpBackend.expectPOST(accessPoint + '/alimentosPedido/new',objetoPost).respond(200,data); //se espera un post
		$httpBackend.expectGET(accessPoint + '/pedido/'+idPedido+'/tiempo/'+tiempo+'/precio/'+precio).respond(200,data); //se espera un get de la carga de empresa por ID 

		crearService.enviarPedido();

		$httpBackend.flush();

	}));

	it('Funcion addAlimentosPedido: Da formato a cada alimento en newPedido.alimentos y REST /alimentosPedido/new. Añade tiempos y precio',inject(function($httpBackend){

		//Defino los campos que necesita la funcion
		crear.newAlimentoPedido.carta_id = 3;
		crear.newAlimentoPedido.nombre = "New  York  cheese  cake";
		crear.newAlimentoPedido.precio = 4.95;
		crear.newAlimentoPedido.cantidad = 6;

		crear.newPedido.alimentos = [];

		crear.newPedido.alimentos.push(crear.newAlimentoPedido);

		//Defino el parametro de la funcion estudiada.
		var idPedido = 1;

		//Post de la peticion REST para añadir alimentos
		var objetoPost = {};
		objetoPost.alimentosPedidos = crear.newPedido.alimentos;

		//Defino el resultado esperado de /pedido/{idPedido}}/tiempo/{tiempo}/precio/{precio}  GET
		var idPedido = 326;
		var tiempo = 600;
		var precio = 29.700000000000003;

		$httpBackend.expectPOST(accessPoint + '/alimentosPedido/new',objetoPost).respond(200); //se espera un post
		$httpBackend.expectGET(accessPoint + '/pedido/'+idPedido+'/tiempo/'+tiempo+'/precio/'+precio).respond(200); //se espera un get de la carga de empresa por ID 

		crearService.addAlimentosPedido(idPedido);

		$httpBackend.flush();

	}));

	it('Funcion tiempoPrecioPedido: Funcion que añade el tiempo y el precio por API REST a un id de pedido dado',inject(function($http,$httpBackend){

		var idPedido = 326;
		var tiempo = 600;
		var precio = 29.7;

		$httpBackend.expectGET(accessPoint + '/pedido/'+idPedido+'/tiempo/'+tiempo+'/precio/'+precio).respond(200); //se espera un get de la carga de empresa por ID 

		crearService.tiempoPrecioPedido(326,600,29.7);

		$httpBackend.flush();

	}));

	it('Funcion resetPedido: Resetea los campos referentes al pedido de la factoria de creacion.',inject(function(){

		//RELLENO LOS CAMPOS DE LA FACTORIA QUE LA FUNCION VA A RESETEAR
		crear.newAlimentoPedido = {};
		crear.newAlimentoPedido.carta_id = 5;
		crear.newAlimentoPedido.nombre = "Hamburguesa";
		crear.newAlimentoPedido.precio = 5.3;
		crear.newAlimentoPedido.cantidad = 5;
		crear.newPedido = {};
		crear.newPedido.alimentos = [];
		crear.autocomplete.selectedItem = 1;
		crear.autocomplete.searchText = "Hamb";


		//EJECUTO LA FUNCION
		crearService.resetPedido();

		//COMPRUEBO QUE SE VACÍAN LOS CAMPOS.
		expect(crear.newAlimentoPedido).toEqual({cantidad:null});
		expect(crear.newPedido).toEqual({});
		expect(crear.autocomplete.selectedItem).toBe(null);
		expect(crear.autocomplete.searchText).toBe("");
		expect(crear.newAlimentoPedido.cantidad).toBe(null);

	}));

	it('Funcion deleteAlimentosPedido: Elimina todos los registros (API REST) de la tabla alimentos_pedido para inicializar una nueva simulacion y llama a un funcion que elimina pedidos',inject(function($httpBackend){


		$httpBackend.expectGET(accessPoint + '/alimentosPedido/eliminar/all').respond(200); //se espera un get de la carga de empresa por ID 
		$httpBackend.expectGET(accessPoint + '/pedido/eliminar/all').respond(200); //se espera un get de la carga de empresa por ID 				

		crearService.deleteAlimentosPedido();

		$httpBackend.flush();

	}));


	it('Funcion deletePedidos: Elimina todos los registros (API REST) de la tabla pedidos para inicializar una nueva simulacion',inject(function($httpBackend){

		$httpBackend.expectGET(accessPoint + '/pedido/eliminar/all').respond(200); //se espera un get de la carga de empresa por ID 				

		crearService.deletePedidos();

		$httpBackend.flush();

	}));

	it('Funcion initializeMesas: Inicializa todas las mesas con estado "Libre"',inject(function($httpBackend){

		$httpBackend.expectGET(accessPoint + '/mesa/' + 1 + '/estado/' + "Libre").respond(200); //se espera un get de la carga de empresa por ID 				
		$httpBackend.expectGET(accessPoint + '/mesa/' + 2 + '/estado/' + "Libre").respond(200);
		$httpBackend.expectGET(accessPoint + '/mesa/' + 3 + '/estado/' + "Libre").respond(200);
		$httpBackend.expectGET(accessPoint + '/mesa/' + 4 + '/estado/' + "Libre").respond(200);

		crearService.initializeMesas();

		$httpBackend.flush();
	}));

});


describe('Modulo de Simulacion', function() {

	var 
		simulacionService, 
		simulacion,
		accessPoint, 
		crear;

	beforeEach(angular.mock.module("elvisHamburguerApp"));

    beforeEach(inject(function (_simulacionService_ , _simulacion_, _accessPoint_, _crear_) {
		
		simulacionService = _simulacionService_;
		simulacion = _simulacion_;
		accessPoint = _accessPoint_;
		crear = _crear_;
		
	}));

	it('Funcion mesaLibre: Funcion que devuelve la primera mesa con estado "Libre"',inject(function(){

		//RELLENO LOS CAMPOS DE LA FACTORIA QUE LA FUNCION VA A UTILIZAR
		simulacion.estadoMesas = [];
		simulacion.estadoMesas.push("EstadoLibre");
		simulacion.estadoMesas.push("EsperandoCuenta");
		simulacion.estadoMesas.push("Libre");
		simulacion.estadoMesas.push("Libre");

		//Resultado esperado
		var resultadoEsperado = 3;

		var result = simulacionService.mesaLibre();

		expect(result).toBe(resultadoEsperado);

	}));	


	it('Funcion iniciarTiempoMesa: Busca el tiempo del pedido que esta en la mesa id = idMesa e inicializa el intervalo total de la mesa donde se encuentra.',inject(function(){

		//RELLENO LOS CAMPOS DE LA FACTORIA QUE LA FUNCION VA A UTILIZAR
		crear.pedidos = [];
		var objetoPedido = {};
		objetoPedido.idPedido = 10;
		objetoPedido.alimentos = [];
		objetoPedido.tiempo = 500;
		objetoPedido.precio = 0;
		objetoPedido.pagado = false;
		objetoPedido.idMesa = 2;
		crear.pedidos.push(objetoPedido);

		//Variable de entrada
		var idMesa = 2;

		//Resultado esperado 
		var tiempoMesa = 500;

		simulacionService.iniciarTiempoMesa(idMesa);

		expect(simulacion.tiemposMesas["mesa2"].tiempoTotal).toBe(tiempoMesa);

	}));

});


window.test = true;
window.iniciarTest();

