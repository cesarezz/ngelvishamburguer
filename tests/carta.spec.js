require("./../src/app.js");

describe('Módulo de Carta',function(){

	var 
		cartaService, 
		carta,
		accessPoint;



	beforeEach(module('elvisHamburguerApp'));

	beforeEach(module(function ($routeProvider) {
         $routeProvider.otherwise(function(){return false;});
    }));


	/*beforeEach(module(function ($routeProvider) {
	    $routeProvider.otherwise(function(){return false;});
	}));*/

	beforeEach(inject(function (_cartaService_ , _carta_, _accessPoint_) {
		
		cartaService = _cartaService_;
		carta = _carta_;
		accessPoint = _accessPoint_;
		
	}));

	describe('Funciones de vista "Ver Carta"',function(){

		it('Debería pedir la carta (api rest) y rellenar la variable data de la factoria Carta con los resultados',function(){

			carta.data = [{"id":1,"nombre":"Suspicious  Mind","descripcion":"maravillosa hamburguesa,¼ de libra, con \r\ncebolla a la plancha, bacon y queso.","precio":5.5},{"id":2,"nombre":"My Way","descripcion":"hamburguesa de ¼ de libra con \r\nlechuga  y  tomate  e  ingredientes  a  escoger entre relish, bacon y queso.","precio":4.0},{"id":3,"nombre":"New  York  cheese  cake","descripcion":"la  original  a  la  vuelta  de  la  esquina","precio":4.95},{"id":4,"nombre":"Refresco","descripcion":"Coca-Cola o Seven-Up","precio":2.0},{"id":5,"nombre":"Chesse Fries","descripcion":"Patatas con queso cheddar y bacon","precio":3.0},{"id":6,"nombre":"Ingrediente Extra","descripcion":"A elegir entre relish  (salsa  de \r\npepinillo dulce picado), bacon y queso cheddar","precio":0.5},{"id":7,"nombre":"Refresco Refill","descripcion":"Coca-Cola o Seven-Up con Refill","precio":5.0}];

			$httpBackend.expectGET(accessPoint  + '/carta/').respond(200, data); //se espera un get de la carga de empresa por ID

			cartaService.pedirCarta();

			$httpBackend.flush();

		});

	});

});