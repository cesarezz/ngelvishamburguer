
require('angular');
require('angular-material');
require('angular-ui-grid');
require('angular-animate');
require("angular-route");
require('angular-mocks');

require('./modules/header/header.module');
require('./modules/carta/carta.module');
require('./modules/crear/crear.module');
require('./modules/simulacion/simulacion.module');

angular.module('elvisHamburguerApp',['ngRoute','ngMaterial','ui.grid','header.module','carta.module','crear.module','simulacion.module'])
.controller('globalController',['headers','headerService',globalController])
.value("accessPoint","http://localhost:8080/elvisHamburgueseria")
.config(['$routeProvider', rutas]);

function globalController(headers,headerService){
	
	this.headers = headers;
	this.headerService = headerService;

}

function rutas($routeProvider){

  $routeProvider
  .when('/carta', {
    templateUrl: './views/carta.views.html',
    controller: 'cartaController',
    resolve: {
          
        initLoad: ['cartaService', function (cartaService) {
               cartaService.pedirCarta();
        }]

        
      }
  }).when('/crear', {
    templateUrl: './views/crear.views.html',
    controller: 'crearController',
    resolve: {
          
        initLoad: ['crearService', function (crearService) {
               crearService.initializarSimulacion();
               crearService.inicializarFactoria();
               crearService.pedirCarta();
        }]

        
      }
  }).when('/simulacion', {
    templateUrl: './views/simulacion.views.html',
    controller: 'simulacionController'
  }).otherwise({
        redirectTo: '/crear'
  });  

}

