/** Modulo header */
require('./simulacion.controller');
require('./simulacion.factory');
require('./simulacion.service');

angular.module('simulacion.module',[])

	.factory('simulacion',SimulacionFactory)

	.service('simulacionService',[
		'simulacion',
		'accessPoint',
		'$http',
		'crear',
		'$filter',
		'$interval',
	SimulacionService])

	.controller('simulacionController',[
		'simulacion',
		'simulacionService',
		'crear',
	simulacionController]);
