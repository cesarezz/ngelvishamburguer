
function SimulacionFactory(){
	
	var Simulacion = function(){

		var self = this;

		self.pedidos = [];

	    self.mode = 'query';
	    self.activated = true;
	    self.estadoJohnny = "JohnnyDescansa";
	    self.johnnyMesa = 0;

	    //self.tiemposMesas = [];
	    var objetoMesa1 = {};
	    objetoMesa1.tiempo = 0;
	    objetoMesa1.tiempoTotal = 0;

	    var objetoMesa2 = {};
	    objetoMesa2.tiempo = 0;
	    objetoMesa2.tiempoTotal = 0;

	    var objetoMesa3 = {};
	    objetoMesa3.tiempo = 0;
	    objetoMesa3.tiempoTotal = 0;

	    var objetoMesa4 = {};
	    objetoMesa4.tiempo = 0;
	    objetoMesa4.tiempoTotal = 0;


	    self.tiemposMesas = {};
	    self.tiemposMesas.mesa1 =objetoMesa1;
	    self.tiemposMesas.mesa2 =objetoMesa2;
	    self.tiemposMesas.mesa3 =objetoMesa3;
	    self.tiemposMesas.mesa4 =objetoMesa4;

	    /*self.tiemposMesas.push(objetoMesa);
	    self.tiemposMesas.push(objetoMesa);
	    self.tiemposMesas.push(objetoMesa);
	    self.tiemposMesas.push(objetoMesa);*/
	    self.modes = [];
		self.showTooltip = true;

		self.estadoMesas = [];
		self.estadoMesas.push("Libre");
		self.estadoMesas.push("Libre");
		self.estadoMesas.push("Libre");
		self.estadoMesas.push("Libre");
	}

	return new Simulacion();

}
