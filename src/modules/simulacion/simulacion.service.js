
function SimulacionService(simulacion,accessPoint,$http,crear,$filter,$interval){
	this.simulacion = simulacion;
	this.accessPoint = accessPoint;
	this.$http = $http;
	this.crear = crear;
	this.$filter = $filter;
	this.$interval = $interval;
}

angular.extend(SimulacionService.prototype,{


	/**
	 * Inicializa los intervalos de cada mesa y comienza la simulacion
	 */
	comenzarSimulacion: function(){

		var self = this;

		self.iniciarTiempoMesas();

		self.buscarSiguientePedido();

	},

	/**
	 * Punto inicial del bucle de la simulacion. Solo entra en esta funcion al iniciar y cuando
	 * "JohnnyDescansa".
	 */
	buscarSiguientePedido: function(){

		var self = this;

		var mesa, idSearch;

		for(var i = 0;  i < self.crear.pedidos.length; i++){

			if(self.crear.pedidos[i].idMesa == null && self.crear.pedidos[i].pagado == false){


				mesa = self.mesaLibre();

				if(mesa != 0){
					idSearch = self.crear.pedidos[i].idPedido;

					self.crear.pedidos[i].idMesa = mesa;

					self.$http
					.get(self.accessPoint + '/pedido/'+idSearch+'/mesa/'+ mesa)
					.then(function(result){

						self.mesaEstadoLibre(result.data);

					},function(error){
						console.error(error);
					});
				}

				break;
			}

		}

		if(idSearch == undefined){

			self.$http
				.get(self.accessPoint + '/johnny/iniciar')
				.then(function(result){

					//self.estadoJohnny();

				},function(error){
					console.error(error);
				});
		}

	},

	/**
	 * Funcion que devuelve la primera mesa con estado "Libre"
	 * @return {Integer} Id de la mesa.
	 */
	mesaLibre: function(){

		var self = this;

		var mesa = 0;

		for(var i = 0;  i < self.simulacion.estadoMesas.length; i++){

			if(self.simulacion.estadoMesas[i].indexOf("Libre") > -1 && self.simulacion.estadoMesas[i].indexOf("Estado") == -1){
				mesa = i + 1;
				break;
			}

		}

		return mesa;
	},

	/**
	 * Pide el estado de Johnny (REST) y según cual sea "tomará nota" (inicia tiempo mesa y 
	 * "estado comiendo"), "llevará la cuenta" (reinicia tiempo mesa y cobra)
	 * o reiniciara el bucle de la simulacion de nuevo (viendo si hay pedidos sin mesa).
	 */
	estadoJohnny: function(){

		var self = this;

		self.$http
		.get(self.accessPoint + '/johnny/estado')
		.then(function(result){

			self.simulacion.estadoJohnny = result.data.accion;

			if(result.data.accion.indexOf("1") > -1 ){
				self.simulacion.johnnyMesa = 1;
			} else if(result.data.accion.indexOf("2") > -1 ){
				self.simulacion.johnnyMesa = 2;
			} else if(result.data.accion.indexOf("3") > -1 ){
				self.simulacion.johnnyMesa = 3;
			} else if(result.data.accion.indexOf("4") > -1 ){
				self.simulacion.johnnyMesa = 4;
			} else {
				self.simulacion.johnnyMesa = 0;
			}

			if (result.data.accion.indexOf("Nuevos comensales en la mesa") > -1){
				
				self.iniciarTiempoMesa(self.simulacion.johnnyMesa);
				self.mesaEstadoComiendo(self.simulacion.johnnyMesa);
				 
			} else if(result.data.accion.indexOf("Johnny lleva la cuenta") > -1){

				/*self.simulacion.tiemposMesas[self.simulacion.johnnyMesa - 1].tiempo = 0;
				self.simulacion.tiemposMesas[self.simulacion.johnnyMesa - 1].tiempoTotal = 0;*/
				self.simulacion.tiemposMesas["mesa"+self.simulacion.johnnyMesa].tiempo = 0;
				self.simulacion.tiemposMesas["mesa"+self.simulacion.johnnyMesa].tiempoTotal = 0;
				self.pagarPedidoIdMesa(self.simulacion.johnnyMesa);
				//self.buscarSiguientePedido();
				//self.mesaEstadoLibre(self.simulacion.johnnyMesa);

			} else {

				self.buscarSiguientePedido();

			}


		},function(error){
			console.error(error);
		});

	},

	/**
	 * Se paga el pedido (tanto en factoria como tabla API REST), el pedido se desvincula de la mesa y
	 * la mesa queda libre cambiando su estado y recalculando bucle de simulacion.
	 * @param  {Integer} Id de la mesa.
	 */
	pagarPedidoIdMesa: function(id){

		var self = this;

		var idPedido;

		for(var j = 0; ( j < self.crear.pedidos.length ); j++){

			if(self.crear.pedidos[j].idMesa == id){
				idPedido = self.crear.pedidos[j].idPedido;
				self.crear.pedidos[j].pagado = true;
				self.crear.pedidos[j].idMesa = undefined;
				break;
			}

		}

		self.simulacion.estadoMesas[id-1] = "Libre";

		self.$http
		.get(self.accessPoint + '/pedido/pagado/' + idPedido)
		.then(function(result){
		
		},function(error){
			console.error(error);
		});

		self.mesaEstadoLibreSinPedidos(id);	

	},

	/**
	 * La mesa con id se pone con el estado a "Libre" (API REST y factoria) y tras esto, se llama a la 
	 * funcion de recalcular estado de Johnny para continuar con la simulación.
	 * @param  {Integer} Id de la mesa.
	 * @return {[type]}
	 */
	mesaEstadoLibre: function(id) {

		var self = this;

		var estado = "EstadoLibre";

		self.simulacion.estadoMesas[id-1] = "Libre";
		
		self.$http
		.get(self.accessPoint + '/mesa/' + id + '/estado/' + estado)
		.then(function(result){
			
			self.estadoJohnny();
			
		},function(error){
			console.error(error);
		});


	},

	/**
	 * Cuando la mesa se queda libre y no hay pedido asignado a la mesa, el estado de la mesa 
	 * es "Libre" y,tras esto, se llama a la funcion de recalcular estado de Johnny para continuar 
	 * con la simulación.
	 * @param  {Integer} Id de la mesa.
	 */
	mesaEstadoLibreSinPedidos: function(id) {

		var self = this;

		var estado = "Libre";

		self.simulacion.estadoMesas[id-1] = "Libre";
		
		self.$http
		.get(self.accessPoint + '/mesa/' + id + '/estado/' + estado)
		.then(function(result){
			
			self.estadoJohnny();
			
		},function(error){
			console.error(error);
		});


	},

	/**
	 * Pone la mesa en estado "EstadoEsperandoCuenta" y,tras esto, se llama a la 
	 * funcion de recalcular estado de Johnny para continuar con la simulación. 
	 * @param  {Integer} Id de la mesa.
	 */
	mesaEstadoEsperandoCuenta: function(id){

		var self = this;

		var estado = "EstadoEsperandoCuenta";

		self.simulacion.estadoMesas[id] = "Esperando Cuenta";

		var idMesa = id + 1;

		self.$http
		.get(self.accessPoint + '/mesa/' + idMesa + '/estado/' + estado)
		.then(function(result){

				self.estadoJohnny();
			
		},function(error){
			console.error(error);
		});


	},

	/**
	 * Pone la mesa en estado "EstadoEsperandoCuenta" y,tras esto, se llama a la 
	 * funcion de recalcular estado de Johnny para continuar con la simulación. 
	 * @param  {Integer} Id de la mesa.
	 */
	mesaEstadoComiendo: function(id){

		var self = this;

		var estado = "EstadoComiendo";

			self.simulacion.estadoMesas[id-1] = "Comiendo";

			self.$http
			.get(self.accessPoint + '/mesa/' + id + '/estado/' + estado)
			.then(function(result){
				
					self.estadoJohnny();
				
			},function(error){
				console.error(error);
			});

	},

	/**
	 * Busca el tiempo del pedido que esta en la mesa id = idMesa e inicializa el intervalo total de la 
	 * mesa donde se encuentra.
	 * @param  {Integer} Id de la mesa.
	 */
	iniciarTiempoMesa: function(id){

		var self = this;
		
		var pedido = angular.copy(self.$filter('filter')(self.crear.pedidos,{idMesa:id},function(actual, expected){
			if(actual === expected){
	          return true;
	        }	
		}));

		self.simulacion.tiemposMesas["mesa"+id].tiempoTotal = pedido[0].tiempo;

	},

	/**
	 * Inicializa los intervalos de cada mesa que se ejecutarán cada 100ms si hay 
	 * comensales "comiendo" en ella.
	 */
	iniciarTiempoMesas: function(){

		var self = this;

		self.intervalMesa1();

		self.intervalMesa2();

		self.intervalMesa3();

		self.intervalMesa4();

	},

	/**
	 * Inicializa el intervalo de la mesa1
	 */
	intervalMesa1:function(){

		var self = this;

		self.$interval(function() {

		  	var j= 0, counter = 0;

		  	if(self.simulacion.tiemposMesas.mesa1.tiempoTotal > 0){

		  		self.simulacion.tiemposMesas.mesa1.tiempo += 100 / self.simulacion.tiemposMesas.mesa1.tiempoTotal;
		      	if (self.simulacion.tiemposMesas.mesa1.tiempo > 100) {
		      		self.simulacion.tiemposMesas.mesa1.tiempo = 100;
		      		self.simulacion.tiemposMesas.mesa1.tiempoTotal = 0;
		      		self.mesaEstadoEsperandoCuenta(0);
		      	}
		        // Incrementally start animation the five (5) Indeterminate,
		        // themed progress circular bars
			    if ( (j < 2) && !self.simulacion.modes[j] && self.simulacion.activated ) {
			      self.simulacion.modes[j] = (j==0) ? 'buffer' : 'query';
			    }
			    if ( self.simulacion.counter++ % 4 == 0 ) j++;
			    // Show the indicator in the "Used within Containers" after 200ms delay
			    if ( j == 2 ) self.simulacion.contained = "indeterminate";
		  	}
	      	

	    }, 100, 0, true);


	},

	/**
	 * Inicializa el intervalo de la mesa2
	 */
	intervalMesa2:function(){

		var self = this;

		self.$interval(function() {

		  	var j= 0, counter = 0;

		  	if(self.simulacion.tiemposMesas.mesa2.tiempoTotal > 0){

		  		self.simulacion.tiemposMesas.mesa2.tiempo += 100 / self.simulacion.tiemposMesas.mesa2.tiempoTotal;
		      	if (self.simulacion.tiemposMesas.mesa2.tiempo > 100) {
		      		self.simulacion.tiemposMesas.mesa2.tiempo = 100;
		      		self.simulacion.tiemposMesas.mesa2.tiempoTotal = 0;
		      		self.mesaEstadoEsperandoCuenta(1);
		      	}
		        // Incrementally start animation the five (5) Indeterminate,
		        // themed progress circular bars
			    if ( (j < 2) && !self.simulacion.modes[j] && self.simulacion.activated ) {
			      self.simulacion.modes[j] = (j==0) ? 'buffer' : 'query';
			    }
			    if ( self.simulacion.counter++ % 4 == 0 ) j++;
			    // Show the indicator in the "Used within Containers" after 200ms delay
			    if ( j == 2 ) self.simulacion.contained = "indeterminate";
		  	}
	      	

	    }, 100, 0, true);


	},

	/**
	 * Inicializa el intervalo de la mesa3
	 */
	intervalMesa3:function(){

		var self = this;

		self.$interval(function() {

		  	var j= 0, counter = 0;

		  	if(self.simulacion.tiemposMesas.mesa3.tiempoTotal > 0){

		  		self.simulacion.tiemposMesas.mesa3.tiempo += 100 / self.simulacion.tiemposMesas.mesa3.tiempoTotal;
		      	if (self.simulacion.tiemposMesas.mesa3.tiempo > 100) {
		      		self.simulacion.tiemposMesas.mesa3.tiempo = 100;
		      		self.simulacion.tiemposMesas.mesa3.tiempoTotal = 0;
		      		self.mesaEstadoEsperandoCuenta(2);
		      	}
		        // Incrementally start animation the five (5) Indeterminate,
		        // themed progress circular bars
			    if ( (j < 2) && !self.simulacion.modes[j] && self.simulacion.activated ) {
			      self.simulacion.modes[j] = (j==0) ? 'buffer' : 'query';
			    }
			    if ( self.simulacion.counter++ % 4 == 0 ) j++;
			    // Show the indicator in the "Used within Containers" after 200ms delay
			    if ( j == 2 ) self.simulacion.contained = "indeterminate";
		  	}
	      	

	    }, 100, 0, true);


	},

	/**
	 * Inicializa el intervalo de la mesa4
	 */
	intervalMesa4:function(){

		var self = this;

		self.$interval(function() {

		  	var j= 0, counter = 0;

		  	if(self.simulacion.tiemposMesas.mesa4.tiempoTotal > 0){

		  		self.simulacion.tiemposMesas.mesa4.tiempo += 100 / self.simulacion.tiemposMesas.mesa4.tiempoTotal;
		      	if (self.simulacion.tiemposMesas.mesa4.tiempo > 100) {
		      		self.simulacion.tiemposMesas.mesa4.tiempo = 100;
		      		self.simulacion.tiemposMesas.mesa4.tiempoTotal = 0;
		      		self.mesaEstadoEsperandoCuenta(3);
		      	}
		        // Incrementally start animation the five (5) Indeterminate,
		        // themed progress circular bars
			    if ( (j < 2) && !self.simulacion.modes[j] && self.simulacion.activated ) {
			      self.simulacion.modes[j] = (j==0) ? 'buffer' : 'query';
			    }
			    if ( self.simulacion.counter++ % 4 == 0 ) j++;
			    // Show the indicator in the "Used within Containers" after 200ms delay
			    if ( j == 2 ) self.simulacion.contained = "indeterminate";
		  	}
	      	

	    }, 100 , 0, true);


	}
	

})