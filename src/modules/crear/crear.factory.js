
function CrearFactory(){
	
	var Crear = function(){

		this.carta = [];
		this.newAlimentoPedido = {};
		this.newPedido = {};
		this.pedidos = [];
		this.autocomplete = {};
		this.autocomplete.selectedItem = null;
		this.autocomplete.searchText = null;
		this.cargando = false;
	}

	return new Crear();

}
