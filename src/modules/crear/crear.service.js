
function CrearService(crear,$filter,$http,accessPoint,$interval,$q){
	this.crear = crear;
	this.$filter = $filter;
	this.$http = $http;
	this.accessPoint = accessPoint;
	this.$interval = $interval;
	this.$q = $q;
}

angular.extend(CrearService.prototype,{

	/**
	* Resetea los campos de la factoria.
	*/
	inicializarFactoria: function(){

		var self = this;

		self.crear.newAlimentoPedido = {};
		self.crear.newPedido = {};
		self.crear.autocomplete.selectedItem = null;
		self.crear.autocomplete.searchText = "";
		self.crear.newAlimentoPedido.cantidad = null;
		self.crear.pedidos = [];

	},


	/**
	* Llamada http REST para pedir la carta y guardar la información .
	*/
	pedirCarta:  function(){
		var self = this;

		//DESCOMENTAR CUANDO FUNCIONE EL SERVICIO
		//self.crear.carta=[];

		self.$http
		.get(self.accessPoint + '/carta/')
		.then(function(result){
			self.crear.carta = result.data; 		
		},function(error){
			console.error(error);
		});

	},

	/**
	* Cuando se modifica el campo Autocomplete, esta funcion recibe el texto y devuelve los elementos
	* de la factoria que contengan dicho texto.
	*/

	/**
	 * Cuando se modifica el campo Autocomplete, esta funcion recibe el texto y devuelve los elementos
	 * de la factoria que contengan dicho texto.
	 * @param  Texto de autocomplete
	 * @return [Object(Carta)]
	 */
	queryBusqueda: function(searchText){
		var self = this;
		
		if(searchText == null) searchText = undefined;

		var results = self.$filter('filter')(self.crear.carta,{ nombre: searchText });
	      
	    return results;
	},

	/**
	 * Cuando en el campo Autocomplete se selecciona un elemento, esta funcion se encarga de colocar el 
	 * elemento en la factoria de creacion
	 * @param  {objeto alimento}
	 */
	selectedItemChange: function(item){
		var self = this;

		if ((item!= undefined) && (item!=null)){

			self.crear.newAlimentoPedido.carta_id = item.id;
			self.crear.newAlimentoPedido.nombre = item.nombre;
			self.crear.newAlimentoPedido.precio = item.precio;

		}

	},

	/**
	* Añade en newPedido de la factoria de creacion el alimento que hubiera en 
	* newAlimentoPedido de la factoria de creacion.
	*/
	addAlimentoPedido: function (){

		var self = this;

		if (!self.crear.newPedido.hasOwnProperty("alimentos")){

			self.crear.newPedido.alimentos = [];

		} 

		if(!self.crear.newAlimentoPedido.hasOwnProperty("carta_id") || (!self.crear.newAlimentoPedido.hasOwnProperty("cantidad") && self.crear.newAlimentoPedido.length == 0)){
			
			alert("Debe insertar un alimento y la cantidad");

		} else {

			var nuevoAlimento = angular.copy(self.crear.newAlimentoPedido);

			var alimento = angular.copy(self.$filter('filter')(self.crear.newPedido.alimentos,{carta_id:self.crear.newAlimentoPedido.carta_id},function(actual, expected){
				if(actual === expected){
		          return true;
		        }	
			}));

			if (alimento.length === 0){

				self.crear.newPedido.alimentos.push(nuevoAlimento);

			} else {

				for(var i = 0; i < self.crear.newPedido.alimentos.length; i++){

					if(self.crear.newPedido.alimentos[i].carta_id == nuevoAlimento.carta_id){

						self.crear.newPedido.alimentos[i].cantidad = self.crear.newPedido.alimentos[i].cantidad + nuevoAlimento.cantidad;

					}

				}

			}

			self.crear.autocomplete.selectedItem = null;
			self.crear.autocomplete.searchText = "";
			self.crear.newAlimentoPedido.cantidad = null;
			
		}
	},

	/**
	 * Peticion rest para recibir un id de pedido y posteriormente llamar a otra funcion pasandole dicho id.
	 */
	enviarPedido:function(){

		var self = this;

		if (!self.crear.newPedido.hasOwnProperty("alimentos")){
			self.crear.newAlimentoPedido = {};
			self.crear.newPedido = {};
		} else {

			//PETICION REST
			self.$http
			.get(self.accessPoint + '/pedido/new/')
			.then(function(result){
				
				var id = result.data.id;
				self.addAlimentosPedido(id);

			},function(error){
				console.error(error);
			});

		}


	},

	/**
	 * Da formato API REST a cada alimento en newPedido.alimentos y se añaden a las tablas 
	 * alimentosPedido (/alimentosPedido/new) con el id del pedido, se calcula tiempo total
	 * y precio total del pedido y se llama a otra funcion con estos dos parámetros
	 * 
	 * @param {integer} Id del pedido nuevo
	 */
	addAlimentosPedido: function(id){

		var self = this;

		debugger

		if(self.crear.newPedido.alimentos.length > 0){

			var objetoPedido = {};
			objetoPedido.idPedido = id;
			objetoPedido.alimentos = [];
			//Cada alimento del pedido suma 500 ms al tiempo de estancia en la mesa
			objetoPedido.tiempo = 0; //self.crear.newPedido.alimentos.length * 500;
			objetoPedido.precio = 0;
			objetoPedido.pagado = false;

			var nombre, cantidad, precio, precioAlimento;

			for(var i = 0; i < self.crear.newPedido.alimentos.length; i++){

				self.crear.newPedido.alimentos[i].pedido_id = id;				

				var objetoAlimento;

				nombre = self.crear.newPedido.alimentos[i].nombre;

				cantidad = self.crear.newPedido.alimentos[i].cantidad;

				precio = self.crear.newPedido.alimentos[i].precio;

				precioAlimentoCantidad = cantidad * precio;

				objetoPedido.precio = objetoPedido.precio + precioAlimentoCantidad;

				objetoPedido.tiempo = objetoPedido.tiempo + (cantidad * 20);

				delete self.crear.newPedido.alimentos[i].nombre;
				delete self.crear.newPedido.alimentos[i].precio;

				/*self.$http
					.post(self.accessPoint + '/alimentosPedido/new',self.crear.newPedido.alimentos[i])
					.then(function(result){	

						objetoAlimento = {};
						objetoAlimento.nombre = nombre;
						objetoAlimento.cantidad = cantidad;
						objetoAlimento.precio = precioAlimentoCantidad;
						objetoPedido.alimentos.push(objetoAlimento);

					},function(error){
						console.error(error);
					});*/
				objetoAlimento = {};
				objetoAlimento.nombre = nombre;
				objetoAlimento.cantidad = cantidad;
				objetoAlimento.precio = precioAlimentoCantidad;
				objetoPedido.alimentos.push(objetoAlimento);

			}

			var objetoPost = {};
			objetoPost.alimentosPedidos = self.crear.newPedido.alimentos;

			self.crear.newPedido = {};

			self.$http
				.post(self.accessPoint + '/alimentosPedido/new',objetoPost)
				.then(function(result){	

					self.resetPedido();
					self.crear.cargando = false;

				},function(error){
					console.error(error);
				});

			//var delay = self.crear.newPedido.alimentos.length * 3000;

			//self.crear.cargando = true;

			/*self.$interval(function(){
				self.resetPedido();
				self.crear.cargando = false;
			}, delay,1)*/

			self.crear.pedidos.push(objetoPedido);
			self.tiempoPrecioPedido(id, objetoPedido.tiempo,objetoPedido.precio);
			//self.precioPedido(id, objetoPedido.precio);

		}

	},

	/**
	 * Funcion que añade el tiempo y el precio por API REST a un id de pedido dado.
	 * 
	 * @param  {Integer} Id del pedido
	 * @param  {Integer} Tiempo del pedido
	 * @param  {Double} Precio del pedido
	 */
	tiempoPrecioPedido: function (id, tiempo,precio){

		var self = this;

		self.$http
		.get(self.accessPoint + '/pedido/'+ id +'/tiempo/'+ tiempo +'/precio/'+precio)
		.then(function(result){

		},function(error){
			console.error(error);
		});

	},

	/*precioPedido: function (id, precio){

		var self = this;

		self.$http
		.get(self.accessPoint + '/pedido/'+ id +'/precio/'+ precio)
		.then(function(result){

		},function(error){
			console.error(error);
		});

	},*/

	/**
	 * Resetea los campos referentes al pedido de la factoria de creacion.
	 */
	resetPedido:function(){

		var self = this;

		self.crear.newAlimentoPedido = {};
		self.crear.newPedido = {};
		self.crear.autocomplete.selectedItem = null;
		self.crear.autocomplete.searchText = "";
		self.crear.newAlimentoPedido.cantidad = null;

	},

	/**
	 * Elimina todos los registros (API REST) de la tabla alimentos_pedido para inicializar una nueva simulacion.
	 * @return {[type]}
	 */
	deleteAlimentosPedido: function(){

		var self = this;

		self.$http
		.get(self.accessPoint + '/alimentosPedido/eliminar/all')
		.then(function(result){

			self.deletePedidos();

		},function(error){
			console.error(error);
		});

	}, 

	/**
	 * Elimina todos los registros (API REST) de la tabla pedidos para inicializar una nueva simulacion
	 */
	deletePedidos: function(){

		var self = this;

		self.$http
		.get(self.accessPoint + '/pedido/eliminar/all')
		.then(function(result){

		},function(error){
			console.error(error);
		});

	}, 

	/**
	* Cambia el estado a "Libre" de todas las mesas de la hamburgueseria
	*/
	initializeMesas: function() {

		var self = this;

		var estado = "Libre";

		var idMesa;

		for(var i = 0; i < 4; i++){

			idMesa = i+1;

			self.$http
			.get(self.accessPoint + '/mesa/' + idMesa + '/estado/' + estado)
			.then(function(result){

			},function(error){
				console.error(error);
			});
		}

	},

	/**
	 * Inicializa mesas con estado a Libre, elimina alimentosPedidos y pedidos de las tablas
	 */
	initializarSimulacion: function(){

		var self = this;

		self.deleteAlimentosPedido();
		self.initializeMesas();

	}



})