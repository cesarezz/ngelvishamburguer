/** Modulo header */
require('./crear.controller');
require('./crear.factory');
require('./crear.service');

angular.module('crear.module',[])

	.factory('crear',CrearFactory)

	.service('crearService',[
		'crear',
		'$filter',
		'$http',
		'accessPoint',
		'$interval',
		'$q',
	CrearService])

	.controller('crearController',[
		'crear',
		'crearService',
	crearController]);
