
function CartaFactory(){
	
	var Carta = function(){

		this.showGridFooter = true;
	    this.showColumnFooter = true;
		this.enableFiltering = true;
		this.enableSorting = true;
		this.enableHorizontalScrollbar = false; 
		this.i18n = 'es'; 
		this.columnDefs = [
		  { name:'Nombre', field: 'nombre', enableCellEdit:false, width: "30%"},
	      { name:'Descripción', field: 'descripcion', enableCellEdit:false, width: "50%"},
		  { name:'Precio', field: 'precio', enableCellEdit:false, width: "18%"}
		];
		this.data = [{nombre:"Hamb1",descripcion:"MyWay",precio:5.91},{nombre:"Hamb2",descripcion:"Cheeseburguer",precio:3}];
	}

	return new Carta();

}
