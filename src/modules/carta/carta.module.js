/** Modulo header */
require('./carta.controller');
require('./carta.factory');
require('./carta.service');

angular.module('carta.module',[])

	.factory('carta',CartaFactory)

	.service('cartaService',[
		'carta',
		'$http',
		'accessPoint',
	CartaService])

	.controller('cartaController',[
		'carta',
		'cartaService',
	CartaController]);
