
function CartaService(carta,$http,accessPoint){
	this.carta = carta;
	this.$http = $http;
	this.accessPoint = accessPoint;
}

angular.extend(CartaService.prototype,{

	/**
	* Llamada http REST para pedir la carta y guardar la información .
	*/
	pedirCarta:  function(){
		var self = this;

		//DESCOMENTAR CUANDO EL SERVICIO FUNCIONE
		self.carta.data=[];

		self.$http
		.get(self.accessPoint + '/carta/')
		.then(function(result){
			self.carta.data = result.data; 				
		},function(error){
			console.error(error);
		});

	}

});