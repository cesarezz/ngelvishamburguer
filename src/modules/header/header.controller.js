/**
 * controlador del header de la aplicación
 * @param  {object} $scope 
 */

function headerController(headers,headerService){
	
	this.headers = headers;
	this.headerService = headerService;

}