/** Modulo header */
require('./header.controller');
require('./header.factory');
require('./header.service');

angular.module('header.module',[])

	.factory('headers',HeaderFactory)

	.service('headerService',[
		'headers',
	HeaderService])

	.controller('headerController',[
		'headers',
		'headerService',
	headerController]);
