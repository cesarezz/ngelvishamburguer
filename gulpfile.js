
/** Importación de plugins **/
var gulp        = require('gulp');
var runSequence = require('gulp-run-sequence');
var Builder     = require('systemjs-builder');
var watch       = require('gulp-watch');
var replace     = require('gulp-replace');
var concat      = require('gulp-concat');
var del         = require('del');
var flatten     = require('gulp-flatten');
//var KarmaServer = require('karma').Server;
var Server = require('karma').Server; 


gulp.task('test', function (done) {
	new Server({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, done).start();
});


/** Construccion de js y css para desarrollo **/
gulp.task('default', function(){
	//runSequence('borrar-dist',['js/css-desarrollo','copiar-archivos','html-desa'],'deploy');
	runSequence('watch','borrar-dist',['js/css-desarrollo','copiar-archivos','html-desa']);
});

gulp.task('js/css-desarrollo',['vendorCSS-desa','vendorJS-desa','appCSS-desa','appJS-desa']);

gulp.task('copiar-archivos',['imagenes','views','templates','fonts','fonts-uigrid','json']);


var filePath = {
  desa : {
    js:         'app.js',
    css:        'app.css',
    vendorCSS:  'vendor.css',
    vendorJS:   'vendor.js'
  }
};

gulp.task('watch', function() {
  gulp.watch('./src/index.html', ['html-desa']);
  gulp.watch('./src/**/*.js', ['appJS-desa']);
  gulp.watch('./src/**/*.css', ['appCSS-desa']);
  gulp.watch(['./src/**/*.html','./src/**/*.png','./src/**/*.jpg'], ['copiar-archivos']);
});

gulp.task('borrar-dist', function(){
  del(['./dist/**/*.js',
        './dist/**/*.css',
        './dist/**/*.html',
        './dist/**/*.png',
        './dist/**/*.jpg',
        './dist/**/*.ico']);
});

gulp.task('vendorCSS-desa', function() {
    return gulp.src([
      './jspm_packages/github/angular/*/angular-material.css',
      './jspm_packages/github/angular/*/angular-material.layouts.css',
      './jspm_packages/github/angular-ui/*/ui-grid.css',
      './jspm_packages/npm/*/css/font-awesome.min.css'
    ])
    .pipe(concat(filePath.desa.vendorCSS))
    .pipe(gulp.dest("./dist/vendor/"))
});

/** Construcción vendor.js Desarrollo **/
gulp.task('vendorJS-desa', function() {
    return gulp.src(['./jspm_packages/system.js','./jspm_packages/system-polyfills.js'])
    .pipe(concat(filePath.desa.vendorJS))
    .pipe(gulp.dest("./dist/vendor/"))
});

/** Construcción app.css Desarrollo **/
gulp.task('appCSS-desa', function() {
    return gulp.src(['./src/**/*.css'])
    .pipe(concat(filePath.desa.css))
    .pipe(gulp.dest("./dist/css/"))
});

/** Construcción app.js Desarrollo **/
gulp.task('appJS-desa', function() {

  var builder = new Builder('','./config.js');
  builder.buildStatic('./src/app','./dist/scripts/'+filePath.desa.js,{ minify: false});

});

gulp.task('imagenes', function() {
    return gulp.src("./src/img/**/*")
    .pipe(gulp.dest("./dist/img/"))
});

/** Copiar las vistas de Angular **/
gulp.task('views', function() {
    return gulp.src([
        "./src/**/*.views.html"
    ])
    .pipe(flatten())
    .pipe(gulp.dest("./dist/views"))
});

/** Copiar las vistas de Angular **/
gulp.task('templates', function() {
    return gulp.src([
        "./src/**/*.template.html"
    ])
    .pipe(flatten())
    .pipe(gulp.dest("./dist/templates/"))
});

/** Copiar las vistas de Angular **/
gulp.task('json', function() {
    return gulp.src([
        "./src/**/*.json"
    ])
    .pipe(flatten())
    .pipe(gulp.dest("./dist/json"))
});

gulp.task('html-desa', function () {

  gulp.src("src/index.html")
    .pipe(replace('System.import("./src/app.js")', 'System.import("./scripts/'+filePath.desa.js+'")'))
    .pipe(gulp.dest('dist'));

});

/** Copiar las fuentes **/
gulp.task('fonts', function() {
    return gulp.src(["./jspm_packages/npm/font-awesome@*/fonts/*.+(woff|woff2|otf|ttf|svg|eot)"])
    .pipe(flatten())
    .pipe(gulp.dest("./dist/fonts/"))
});

gulp.task('fonts-uigrid', function() {
    return gulp.src(["./jspm_packages/github/angular-ui/**/*.+(eot|svg|ttf|woff)"])
    .pipe(flatten())
    .pipe(gulp.dest("./dist/vendor/"))
});


